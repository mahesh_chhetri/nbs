
users = {
	id:uniqueID,
	name:String,
	email:String,
	password:string,
	contact:Number,
	address:String,
	following:[{
		profile_name:
		profile_ID:
		usersID:
		date:
	}]
}

news = {
	id:uniqueID,
	images:[s3:bucket:store:id],
	headline:String,
	body:String,
	published_by:Schema.users.id
	published_on:Date,
	likes:Number,
	video:String,
	audio:String,
	childern:[newsID]
}

news_comments = {
	id:uniqueID,
	news:Schema.news.ID
	user:{Schema.users.name,Schema.users.ID},
	body:String,
	date:Date
}

profile_reporters = {
	intro:String,
	followers:Number,
	education:String,
	Experience:String,
	skills:String
}

profile_employers = {
	company:String,
	address:String,
	Designation:String,
	followers:Number
}

profile_organizers = {
	followers:Number,
	company:String,
}


