// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','appRoutes','reporter']);
app.controller("appController",
  ['$scope', '$ionicPopup', '$ionicModal', '$timeout',
    function ($scope, $ionicPopup, $ionicModal, $timeout) {



      $ionicModal.fromTemplateUrl('my-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
      });

      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          $scope.isLoggedIn = true;
          $scope.result = user;
          $scope.$apply();
        } else {
          $scope.isLoggedIn = false;
          $scope.$apply();
        }
      });

      $scope.user = {
        email: '',
        password: '',
      };


      // Sign in with email and password

      $scope.signInWithEmailAndPassword = function () {
        firebase.auth().signInWithEmailAndPassword($scope.user.email, $scope.user.password)
          .then(function (res) {
            $scope.errorMessage = "";
          })
          .catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
            $scope.errorMessage = errorMessage;
            $scope.$apply();
          });
      }


      // Login
      $scope.googleLogin = function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        firebase.auth().signInWithPopup(provider).then((result) => {
          $scope.result = result;
          $scope.$apply();
        });
      }

      $scope.logout = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Logout',
          template: 'Are you sure you want to logout?'
        });

        confirmPopup.then((res) => {
          if (res) {
            $scope.modal.show();
            $timeout(() => {
              firebase.auth().signOut().then((res) => {
                $scope.result = '';
                $scope.modal.hide();
              }).catch(function (error) {
                console.log("error", error);
                $scope.modal.hide();
              });
            }, 3000);

          } else {
            // console.log('You are not sure');
          }
        });
      }
    }]);


app.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
})
