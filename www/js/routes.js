var appRoutes = angular.module('appRoutes', []);
appRoutes.config(['$stateProvider','$urlRouterProvider', function ($stateProvider,$urlRouterProvider) {
    $stateProvider
    $stateProvider
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })
        .state('tab.home', {
          url: '/home',
          views: {
            'tab-home': {
              templateUrl: 'templates/home.html'
            //   controller: 'TrackCtrl'
            }
          }
        })
        .state('tab.profile', {
          url: '/profile',
          views: {
            'tab-profile': {
              templateUrl: 'templates/profile.html'
            //   controller: 'profileCtrl'
            }
          }
        })
        .state('tab.reporter-list', {
            url: '/reporter-list',
            views: {
              'tab-reporter-list': {
                templateUrl: 'modules/reporter/index.html',
                controller: 'reporterListCtrl'
              }
            }
          });

        $urlRouterProvider.otherwise('/tab/home');
        
        // .state('tab.buddies', {
        //   url: '/buddies',
        //   views: {
        //     'buddies': {
        //       templateUrl: 'templates/buddies.html',
        //       controller: 'BuddiesCtrl'
        //     }
        //   }
        // })
        // .state('tab.checklist', {
        //   url: '/checklist',
        //   views: {
        //     'checklist': {
        //       templateUrl: 'templates/checklist.html',
        //       controller: 'ChecklistCtrl'
        //     }
        //   }
        // });
}])
